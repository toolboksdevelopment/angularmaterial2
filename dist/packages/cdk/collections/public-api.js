/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
export { ArrayDataSource } from './array-data-source';
export { DataSource } from './data-source';
export { getMultipleValuesInSingleSelectionError, SelectionModel } from './selection';
export { UniqueSelectionDispatcher, } from './unique-selection-dispatcher';
//# sourceMappingURL=public-api.js.map