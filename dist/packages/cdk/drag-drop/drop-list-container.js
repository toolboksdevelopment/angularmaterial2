/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { InjectionToken } from '@angular/core';
/**
 * @record
 * @template T
 */
export function CdkDropListContainer() { }
/**
 * DOM node that corresponds to the drop container.
 * @type {?}
 */
CdkDropListContainer.prototype.element;
/**
 * Arbitrary data to attach to all events emitted by this container.
 * @type {?}
 */
CdkDropListContainer.prototype.data;
/**
 * Unique ID for the drop zone.
 * @type {?}
 */
CdkDropListContainer.prototype.id;
/**
 * Direction in which the list is oriented.
 * @type {?}
 */
CdkDropListContainer.prototype.orientation;
/**
 * Locks the position of the draggable elements inside the container along the specified axis.
 * @type {?}
 */
CdkDropListContainer.prototype.lockAxis;
/**
 * Whether starting a dragging sequence from this container is disabled.
 * @type {?}
 */
CdkDropListContainer.prototype.disabled;
/**
 * Starts dragging an item.
 * @type {?}
 */
CdkDropListContainer.prototype.start;
/**
 * Drops an item into this container.
 * \@param item Item being dropped into the container.
 * \@param currentIndex Index at which the item should be inserted.
 * \@param previousContainer Container from which the item got dragged in.
 * @type {?}
 */
CdkDropListContainer.prototype.drop;
/**
 * Emits an event to indicate that the user moved an item into the container.
 * \@param item Item that was moved into the container.
 * \@param pointerX Position of the item along the X axis.
 * \@param pointerY Position of the item along the Y axis.
 * @type {?}
 */
CdkDropListContainer.prototype.enter;
/**
 * Removes an item from the container after it was dragged into another container by the user.
 * \@param item Item that was dragged out.
 * @type {?}
 */
CdkDropListContainer.prototype.exit;
/**
 * Figures out the index of an item in the container.
 * \@param item Item whose index should be determined.
 * @type {?}
 */
CdkDropListContainer.prototype.getItemIndex;
/** @type {?} */
CdkDropListContainer.prototype._sortItem;
/** @type {?} */
CdkDropListContainer.prototype._draggables;
/** @type {?} */
CdkDropListContainer.prototype._getSiblingContainerFromPosition;
/** @type {?} */
CdkDropListContainer.prototype._canReturnItem;
/** @type {?} */
CdkDropListContainer.prototype._positionCache;
/** *
 * Injection token that is used to provide a CdkDropList instance to CdkDrag.
 * Used for avoiding circular imports.
  @type {?} */
export const CDK_DROP_LIST_CONTAINER = new InjectionToken('CDK_DROP_LIST_CONTAINER');
//# sourceMappingURL=drop-list-container.js.map