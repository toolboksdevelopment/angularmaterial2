/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { Directive, TemplateRef } from '@angular/core';
export class CdkStepLabel {
    /**
     * @param {?} template
     */
    constructor(template) {
        this.template = template;
    }
}
CdkStepLabel.decorators = [
    { type: Directive, args: [{
                selector: '[cdkStepLabel]',
            },] },
];
/** @nocollapse */
CdkStepLabel.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /**
     * \@docs-private
     * @type {?}
     */
    CdkStepLabel.prototype.template;
}
//# sourceMappingURL=step-label.js.map