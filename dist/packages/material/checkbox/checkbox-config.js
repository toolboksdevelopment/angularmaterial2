/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/** @typedef {?} */
var MatCheckboxClickAction;
export { MatCheckboxClickAction };
/** *
 * Injection token that can be used to specify the checkbox click behavior.
  @type {?} */
export const MAT_CHECKBOX_CLICK_ACTION = new InjectionToken('mat-checkbox-click-action');
//# sourceMappingURL=checkbox-config.js.map