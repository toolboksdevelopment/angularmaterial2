/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Stripped-down HammerJS annotations to be used within Material, which are necessary,
 * because HammerJS is an optional dependency. For the full annotations see:
 * https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/hammerjs/index.d.ts
 */
/**
 * \@docs-private
 * @record
 */
export function HammerInput() { }
/** @type {?} */
HammerInput.prototype.preventDefault;
/** @type {?} */
HammerInput.prototype.deltaX;
/** @type {?} */
HammerInput.prototype.deltaY;
/** @type {?} */
HammerInput.prototype.center;
/**
 * \@docs-private
 * @record
 */
export function HammerStatic() { }
/* TODO: handle strange member:
new(element: HTMLElement | SVGElement, options?: any): HammerManager;
*/
/** @type {?} */
HammerStatic.prototype.Pan;
/** @type {?} */
HammerStatic.prototype.Swipe;
/** @type {?} */
HammerStatic.prototype.Press;
/**
 * \@docs-private
 * @record
 */
export function Recognizer() { }
/* TODO: handle strange member:
new(options?: any): Recognizer;
*/
/** @type {?} */
Recognizer.prototype.recognizeWith;
/**
 * \@docs-private
 * @record
 */
export function RecognizerStatic() { }
/**
 * \@docs-private
 * @record
 */
export function HammerInstance() { }
/** @type {?} */
HammerInstance.prototype.on;
/** @type {?} */
HammerInstance.prototype.off;
/**
 * \@docs-private
 * @record
 */
export function HammerManager() { }
/** @type {?} */
HammerManager.prototype.add;
/** @type {?} */
HammerManager.prototype.set;
/** @type {?} */
HammerManager.prototype.emit;
/** @type {?} */
HammerManager.prototype.off;
/** @type {?} */
HammerManager.prototype.on;
/**
 * \@docs-private
 * @record
 */
export function HammerOptions() { }
/** @type {?|undefined} */
HammerOptions.prototype.cssProps;
/** @type {?|undefined} */
HammerOptions.prototype.domEvents;
/** @type {?|undefined} */
HammerOptions.prototype.enable;
/** @type {?|undefined} */
HammerOptions.prototype.preset;
/** @type {?|undefined} */
HammerOptions.prototype.touchAction;
/** @type {?|undefined} */
HammerOptions.prototype.recognizers;
/** @type {?|undefined} */
HammerOptions.prototype.inputClass;
/** @type {?|undefined} */
HammerOptions.prototype.inputTarget;
//# sourceMappingURL=gesture-annotations.js.map