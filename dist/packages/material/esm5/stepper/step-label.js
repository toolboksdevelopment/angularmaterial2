/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import * as tslib_1 from "tslib";
import { Directive } from '@angular/core';
import { CdkStepLabel } from '@angular/cdk/stepper';
var MatStepLabel = /** @class */ (function (_super) {
    tslib_1.__extends(MatStepLabel, _super);
    function MatStepLabel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MatStepLabel.decorators = [
        { type: Directive, args: [{
                    selector: '[matStepLabel]',
                },] },
    ];
    return MatStepLabel;
}(CdkStepLabel));
export { MatStepLabel };
//# sourceMappingURL=step-label.js.map