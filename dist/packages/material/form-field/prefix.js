/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { Directive } from '@angular/core';
/**
 * Prefix to be placed the the front of the form field.
 */
export class MatPrefix {
}
MatPrefix.decorators = [
    { type: Directive, args: [{
                selector: '[matPrefix]',
            },] },
];
//# sourceMappingURL=prefix.js.map