/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/** @typedef {?} */
var MenuPositionX;
export { MenuPositionX };
/** @typedef {?} */
var MenuPositionY;
export { MenuPositionY };
//# sourceMappingURL=menu-positions.js.map